#ifndef __VREPSETUP__
#define __VREPSETUP__

#include "ros/ros.h"
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosEnableSubscriber.h"
#include "vrep_common/simRosGetObjectHandle.h"
#include "std_msgs/Float64.h"
#include "v_repConst.h"
#include "vrep_playground/pugixml.hpp"
#include <sensor_msgs/JointState.h>
#include <kdl_wrapper/kdl_wrapper.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

class setup
{
  public:
    setup(ros::NodeHandle node)
    {
      n = node;
    };

    bool initPublisher(const char * pubName, int buffer, int handle , int streamCmd);
    bool initSubscriber(const char * pubName, int buffer, int handle , int streamCmd);
    bool getHandle(const char * name, int & handle);

  protected:
    ros::NodeHandle n;
};

class robot : public setup
{
  public:
    robot(ros::NodeHandle n) : setup(n){};
    robot(ros::NodeHandle n, std::string givenName,
      std::string jointStateTopicName, std::string jointVelocityTopicName,
      std::string baseLinkName, std::string tipLinkName) : setup(n)
    {
      kdl_wrapper = new KDLWrapper;
      name = givenName;
      vrepPrefix = "/vrep/";

      ROS_DEBUG("Successfully created a robot object with name %s!", name.c_str());

      if(!kdl_wrapper->init(baseLinkName, tipLinkName))
      {
          ROS_ERROR("Error initializing KDL Wrapper");
      }

      if(!initPublisher((name + jointStateTopicName).c_str(), 1, sim_handle_all, simros_strmcmd_get_joint_state))
      {
        ROS_ERROR("Error initializing state publishers for robot %s", name.c_str());
      }

      if(!initializeJointSubscribers(name + jointVelocityTopicName))
      {
        ROS_ERROR("Error initializing joint velocity subscribers for robot %s", name.c_str());
      }

      jointStateSub = n.subscribe(vrepPrefix + name + jointStateTopicName, 1, &robot::jointStateCallBack, this);
    }

    std::string getName();
    KDL::JntArray jointValues();
    int nrJoints();
    KDLWrapper * kdl_wrapper;

    bool publishJointVelocityTarget(std::vector<float> jointVelocities);

  protected:
    std::string name;
    KDL::JntArray jntValues;
    ros::Subscriber jointStateSub;
    std::vector<ros::Publisher> jointVelPubArray;
    std::vector<std::string> jointNamesArray;
    std::string vrepPrefix;

    bool initializeJointSubscribers(std::string jointTopicName);
    void jointStateCallBack(const sensor_msgs::JointState::ConstPtr& msg);
};

class playground : public setup
{
  public:
    playground(ros::NodeHandle node) : setup(node){};

    bool loadConfig();
    robot * getRobot(char* name);
    int nrRobots();


  private:
    std::vector<robot*> robots;


};


#endif
