#include "ros/ros.h"
#include "vrep_playground/vrep_setup.hpp"
#include <tf_conversions/tf_kdl.h>
#include <tf/transform_broadcaster.h>
#include <ros/console.h>

class controller
{
  public:
    controller(){}
    std::vector<float> run(KDL::JntArray currentState, KDL::JntArray desiredState);
};

std::vector<float> controller::run(KDL::JntArray currentState, KDL::JntArray desiredState)
{
  std::vector<float> u(currentState.rows());

  for(int i=0; i < currentState.rows(); i++)
  {
    //ROS_DEBUG("Current state: %.2f", currentState(i));
    //ROS_DEBUG("Desired state: %.2f", desiredState(i));
    u[i] = -(currentState(i)-desiredState(i));
  }
  //std::cin.get();

  return u;
}


int main(int argc, char ** argv)
{
  ros::init(argc, argv, "playground");
  ros::NodeHandle n;
  ros::Rate rate(10);
  playground pObject(n);

  //SET DEBUG VERBOSITY
  if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) )
  {
     ros::console::notifyLoggerLevelsChanged();
  }

  if(!pObject.loadConfig())
  {
    ROS_ERROR("Error initializing the playground");
    return false;
  }

  // From here on, we have a set of joint states and the capability to send velocity commands to the robot
  ROS_DEBUG("Number of robots initialized: %d", pObject.nrRobots());
  ROS_INFO("Successfully initialized joint state publisher, joint velocity subscriber and kinematic chain from URDF. All set to go!");
  std::cout << "Press enter to continue..." << std::endl;
  std::cin.get();

  KDL::JntArray jointArray;
  KDL::JntArray jointDesired;
  std::vector<float> jointVel(0);
  controller arm_control;
  robot * currRobot;

  currRobot = pObject.getRobot("LeftArm");

  while(ros::ok())
  {
    ros::spinOnce();

    jointArray = currRobot->jointValues();
    jointDesired.resize(currRobot->nrJoints());

    for(int i = 0; i < currRobot->nrJoints(); i++)
    {
      jointDesired(i) = 0;
    }

    jointVel.resize(currRobot->nrJoints());
    KDL::Frame eef;

    if(currRobot->kdl_wrapper->fk_solver_pos->JntToCart(jointArray, eef) < 0)
    {
      ROS_ERROR("Error computing forward kinematics");
      return false;
    }

    std::cout << "Eef position: " << eef.p.x() << " " << eef.p.y() << " " << eef.p.z() << std::endl;

    // Controller here

    jointVel = arm_control.run(jointArray, jointDesired);

    // End of controller code

    currRobot->publishJointVelocityTarget(jointVel);

    rate.sleep();
  }


  return 0;
}
