#include "vrep_playground/vrep_setup.hpp"

bool setup::initPublisher(const char * pubName, int buffer, int handle , int streamCmd)
{
  ros::ServiceClient client_enablePublisher = n.serviceClient<vrep_common::simRosEnablePublisher>("/vrep/simRosEnablePublisher");
	vrep_common::simRosEnablePublisher srv_enablePublisher;

  ROS_DEBUG("VALUES FOR THE JOINT STATE PUBLISHER");
  ROS_DEBUG("TOPIC NAME: %s", pubName);
  ROS_DEBUG("BUFFER SIZE: %d", buffer);
  ROS_DEBUG("STREAM COMMAND: %d", streamCmd);
  ROS_DEBUG("OBJECT HANDLE: %d", handle);

	srv_enablePublisher.request.topicName=pubName;
	srv_enablePublisher.request.queueSize=buffer;
	srv_enablePublisher.request.streamCmd=streamCmd;
	srv_enablePublisher.request.auxInt1=handle;

  // A failed service call or an empty effective topic name means that the operation has failed.
  if (!(client_enablePublisher.call(srv_enablePublisher) && (srv_enablePublisher.response.effectiveTopicName.length() != 0)))
  {
    ROS_ERROR("Failed to create a vrep publisher with name %s!", pubName);
    return false;
  }

  ROS_DEBUG("Successfully created a vrep publisher with name %s!", pubName);
  return true;
}

bool setup::initSubscriber(const char * pubName, int buffer, int handle , int streamCmd)
{
  ros::ServiceClient client_enableSubscriber = n.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
  vrep_common::simRosEnableSubscriber srv_enableSubscriber;

  srv_enableSubscriber.request.topicName = pubName;
  srv_enableSubscriber.request.queueSize = buffer;
  srv_enableSubscriber.request.streamCmd = streamCmd;
  srv_enableSubscriber.request.auxInt1 = handle;

  if (!(client_enableSubscriber.call(srv_enableSubscriber) && (srv_enableSubscriber.response.subscriberID != -1)))
  {
    ROS_ERROR("Failed to create a vrep subscriber with name %s!", pubName);
    return false;
  }

  ROS_DEBUG("Successfully created a vrep subscriber with name %s!", pubName);
  return true;
}

bool setup::getHandle(const char * name, int & handle)
{
  ros::ServiceClient client_getHandle = n.serviceClient<vrep_common::simRosGetObjectHandle>("/vrep/simRosGetObjectHandle");
  vrep_common::simRosGetObjectHandle srv_getObjectHandle;

  srv_getObjectHandle.request.objectName = name;

  if (!(client_getHandle.call(srv_getObjectHandle) && srv_getObjectHandle.response.handle != -1))
  {
    ROS_ERROR("Could not get object handle for object name %s!", name);
    return false;
  }

  handle = srv_getObjectHandle.response.handle;
  return true;
}

void robot::jointStateCallBack(const sensor_msgs::JointState::ConstPtr& msg)
{
  ROS_DEBUG_ONCE("\nEntered call back for robot %s\nRobot has %d joints\n", name.c_str(), nrJoints());
  int misses = 0;
  std::vector<std::string>::iterator iter;

  for (int i = 0; i < nrJoints() + misses && i < msg->position.size(); i++)
  {
    iter = std::find(jointNamesArray.begin(), jointNamesArray.end(), msg->name[i]);
    if(iter!=jointNamesArray.end())
    {
      ROS_DEBUG_ONCE("Joint val: %.2f", msg->position[i]);
      this->jntValues(std::distance(jointNamesArray.begin(), iter)) = msg->position[i];
      ROS_DEBUG_ONCE("Assigned to position %d: %.2f", (int)std::distance(jointNamesArray.begin(), iter), jntValues(std::distance(jointNamesArray.begin(), iter)));
    }
    else
    {
      misses++;
    }
  }
}

bool robot::initializeJointSubscribers(std::string jointTopicName)
{
  int jointHandle = -1;
  int misses = 0;
  std::stringstream ss;
  KDL::Chain chain = kdl_wrapper->getKDLChain();
  jointVelPubArray.resize(chain.getNrOfJoints());
  jointNamesArray.resize(chain.getNrOfJoints());
  jntValues.resize(chain.getNrOfJoints());

  for(int i = 0; i < chain.getNrOfJoints(); i++)
  {
    jntValues(i) = 0;
  }

  ROS_DEBUG("Joint arrays have size %d", chain.getNrOfJoints());

  for (int i = 0; i < chain.getNrOfJoints() + misses; i++)
  {
    if(chain.getSegment(i).getJoint().getType() != KDL::Joint::None)
    {
      ss << i + 1 - misses;
      getHandle(chain.getSegment(i).getJoint().getName().c_str(), jointHandle);
      ROS_DEBUG("Segment joint name: %s", chain.getSegment(i).getJoint().getName().c_str());
      initSubscriber((jointTopicName + ss.str()).c_str(), 1, jointHandle, simros_strmcmd_set_joint_target_velocity);
      jointVelPubArray[i-misses] = n.advertise<std_msgs::Float64>((vrepPrefix + jointTopicName + ss.str()).c_str(), 1000);
      jointNamesArray[i-misses] = chain.getSegment(i).getJoint().getName();
      ss.str(std::string());
    }
    else
    {
      misses++;
    }
  }

  return true;
}

bool robot::publishJointVelocityTarget(std::vector<float> jointVelocities)
{
  std_msgs::Float64 msg;
  for (int i=0; i < nrJoints() && i < jointVelocities.size(); i++)
  {
    msg.data = jointVelocities[i];
    jointVelPubArray[i].publish(msg);
  }
}

int robot::nrJoints()
{
  return kdl_wrapper->getKDLChain().getNrOfJoints();
}

std::string robot::getName()
{
  return name;
}

KDL::JntArray robot::jointValues()
{
  return jntValues;
}

bool playground::loadConfig()
{
  std::ifstream xmlPath;

  xmlPath.open("/home/diogo/Dropbox/PhD/Code/catkin_workspace/src/vrep_playground/input/config.xml");

  if(!xmlPath.is_open())
  {
    ROS_ERROR("Config file failed to open");
    return false;
  }

  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load(xmlPath);

  if (!result)
  {
    ROS_ERROR("Config load error: %s", result.description());
    return false;
  }

  // Traverse the tree: For every <robot>, load its <robotName>, <vrepRosPublishers>, <vrepRosSubscribers> and <robotInfo>.
  pugi::xml_node node;
  node = doc.child("config");
  std::string robotName, jointStateTopicName, jointVelocityTopicName, baseLinkName, tipLinkName;
  robot * currRobot;

  ROS_DEBUG("Travessing tree");
  try
  {
    for(pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it)
    {
      ROS_DEBUG("Node name: %s", it->name());
      if(strcmp(it->name(), "robot") == 0)
      {
        robotName = it->child("robotName").text().get();
        jointStateTopicName = it->child("vrepRosPublishers").child("jointStateTopicName").text().get();
        jointVelocityTopicName = it->child("vrepRosSubscribers").child("jointVelocityTopicName").text().get();
        baseLinkName = it->child("robotInfo").child("baseLinkName").text().get();
        tipLinkName = it->child("robotInfo").child("tipLinkName").text().get();

        ROS_DEBUG("Adding robot '%s':", robotName.c_str());
        currRobot = new robot(n, robotName, jointStateTopicName, jointVelocityTopicName, baseLinkName, tipLinkName);

        robots.push_back(currRobot);
      }
    }
  }
  catch(std::string e)
  {
      ROS_ERROR("Error loading configuration: %s", e.c_str());
      return false;
  }

  return true;
}

int playground::nrRobots()
{
  return robots.size();
}

robot * playground::getRobot(char * name)
{
  for (int i=0; i < robots.size(); i++)
  {
    if (robots[i]->getName() == name)
    {
      return robots[i];
    }
  }
  return NULL;
}
